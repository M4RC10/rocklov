describe "POST /signup" do
  context "novo usuario" do
    before(:all) do
      payload = { name: "Pitty", email: "pitty@bol.com.br", password: "qaninja" }
      MongoDB.new.remove_user(payload[:email])

      @result = Signup.new.create(payload)
    end

    it "valida status code" do
      expect(@result.code).to eql 200
    end

    it "valida id do usuário" do
      expect(@result.parsed_response["_id"].length).to eql 24     # verifica se o campo id esta retornando 24 digitos
    end
  end

  context "usuario ja existe" do
    before(:all) do
      # dado de que eu tenha um novo usuario
      payload = { name: "Joao da Silva", email: "joao@ig.com.br", password: "qaninja" }
      MongoDB.new.remove_user(payload[:email])

      # e o email desse usuario ja foi cadastrado no sistema
      Signup.new.create(payload)

      # quando faco uma requisicao para a rota /signup
      @result = Signup.new.create(payload)
    end

    it "deve retornar 409" do
      # deve retornar 409
      expect(@result.code).to eql 409
    end

    it "deve retornar mensagem" do
      # deve retornar a mensagem "Email already exists :("
      expect(@result.parsed_response["error"]).to eql "Email already exists :("
    end
  end
end

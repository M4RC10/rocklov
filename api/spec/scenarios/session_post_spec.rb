# DRY Don't Repeat Yourself => Não se repita

describe "POST /sessions" do
  context "login com sucesso" do
    before(:all) do
      payload = { email: "marcio@hotmail.com", password: "qaninja" }
      @result = Sessions.new.login(payload)
    end

    it "valida status code" do
      expect(@result.code).to eql 200
    end

    it "valida id do usuário" do
      expect(@result.parsed_response["_id"].length).to eql 24     # verifica se o campo id esta retornando 24 digitos
    end

    #puts result.parsed_response    # pegar todos os campos no resultado no formato hash
    #puts result.parsed_response["_id"]            # pegar um campo expecífico no resultado no formato hash
    #puts result.parsed_response.class
  end

  # examples = [
  #   {
  #     title: "senha incorreta",
  #     payload: { email: "marcio@gmail.com", password: "123456" },
  #     code: 401,
  #     error: "Unauthorized",
  #   },
  #   {
  #     title: "usuario nao existe",
  #     payload: { email: "404@gmail.com", password: "123456" },
  #     code: 401,
  #     error: "Unauthorized",
  #   },
  #   {
  #     title: "email em branco",
  #     payload: { email: "", password: "123456" },
  #     code: 412,
  #     error: "required email",
  #   },
  #   {
  #     title: "sem o campo email",
  #     payload: { password: "123456" },
  #     code: 412,
  #     error: "required email",
  #   },
  #   {
  #     title: "senha em branco",
  #     payload: { email: "marcio@gmail.com", password: "" },
  #     code: 412,
  #     error: "required password",
  #   },
  #   {
  #     title: "sem o campo senha",
  #     payload: { email: "marcio@gmail.com" },
  #     code: 412,
  #     error: "required password",
  #   },
  # ]

  examples = Helpers::get_fixture("login")

  examples.each do |e|
    context "#{e[:title]}" do
      before(:all) do
        @result = Sessions.new.login(e[:payload])
      end

      it "valida status code #{e[:code]}" do
        expect(@result.code).to eql e[:code]
      end

      it "valida id do usuário" do
        expect(@result.parsed_response["error"]).to eql e[:error]     # verifica retorno do  erro de senha inválida
      end

      #puts result.parsed_response    # pegar todos os campos no resultado no formato hash
      #puts result.parsed_response["_id"]            # pegar um campo expecífico no resultado no formato hash
      #puts result.parsed_response.class
    end
  end
end

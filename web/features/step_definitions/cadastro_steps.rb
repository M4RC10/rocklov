Dado("que acesso a página de cadastro") do # Este passo é universal, ou seja, é utilizado em outros cenários
  @signup_page.open
end

# primeiro cenário

Quando("submeto o seguinte formulario de cadastro:") do |table|
  # table is a Cucumber::MultilineArgument::DataTable

  #log table.hashes  # feita para entender como o cucumber converte uma tabela em PIPE(data table), para um objeto do Ruby

  user = table.hashes.first

  # log user

  MongoDB.new.remove_user(user[:email])
  @signup_page.create(user)
end

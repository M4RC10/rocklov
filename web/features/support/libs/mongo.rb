require "mongo"

Mongo::Logger.logger = Logger.new("./logs/mongo.log")

class MongoDB
  attr_accessor :client, :users, :equipos

  def initialize
    @client = Mongo::Client.new(CONFIG["mongo"])   # cria conexão com o Robomongo
    @users = client[:users]  # acessa a collection "users" do robomongo, onde estão sendo gravados os cadastros de usuário
    @equipos = client[:equipos]  # acessa a collection "equipos" do robomongo, onde estão sendo gravados os cadastros de equipamentos
  end

  def drop_danger
    @client.database.drop
  end

  def insert_users(docs)
    @users.insert_many(docs)
  end

  def remove_user(email)
    @users.delete_many({ email: email }) # query para deletar o usuário com o email correspondênte, para não dar erro de duplicidade na execução
  end

  def get_user(email)
    #metodo para retornar o user id do usuário que será chamado
    user = @users.find({ email: email }).first
    return user[:_id]
  end

  def remove_equipo(name, email)
    user_id = get_user(email)
    @equipos.delete_many({ name: name, user: user_id }) # query para deletar o equipamento com o nome correspondênte, para não dar erro de duplicidade na execução
  end
end
